## Node Js Api | MySQL


### Libraries

* **Node Js**: For Api
* **body-parser**: Version ^1.19.0
* **express**: Version ^4.17.1.
* **express-validator**: Vestion ^6.4.0.
* **mysq**: Version ^2.18.1


### Installation and Configuration

**1. Enter git clone and the repository URL at your command line::** 
~~~
git clone https://bhupesh19921@bitbucket.org/bhupesh-kushwaha/nodejs-restful-api.git
~~~

**2. Goto nodejs-restful-api directory :** 
~~~
npm install
~~~

**3. Change mysql database connection from `index.js` :** 
~~~
host: 'localhost',

user: 'YOUR USER',

password: 'YOUR PASSWORS',

database: 'YOUR DATABASE NAME'
~~~

**4. Finally, Start your server:**
~~~
node index
~~~