const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mysql = require('mysql');
const { check, validationResult } = require('express-validator');

const cors = require('cors');
app.use(cors());
 
// parse application/json
app.use(bodyParser.json());
 
//create database connection
const conn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root@123',
  database: 'react_node_db'
});
 
//connect to database
conn.connect((err) =>{
  if(err) throw err;
  console.log('Mysql Connected...');
});
 
//login user
app.post('/api/login',[
    check('email').isEmail(),
    check('password').isLength({ max: 8 })
  ],
  (req, res) => {
    const errors = validationResult(req);
    
    if (!errors.isEmpty()) {
      return res.send(JSON.stringify({
        "status": 422, 
        "error": errors.array(), 
        "response": req.body.email + "===" + req.body.password
      }));
    }

    let sql = "SELECT * FROM users WHERE email='"+req.body.email+"' and password='"+req.body.password+"'";
  
    let query = conn.query(sql, (err, results) => {
      if(err) throw err;
  
      res.send(JSON.stringify({"status": 200, "error": sql, "response": results}));
    });
});

//show all user
app.get('/api/user',(req, res) => {
  let sql = "SELECT * FROM users";

  let query = conn.query(sql, (err, results) => {
    if(err) throw err;

    res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
  });
});
 
//show single user
app.get('/api/user/:id',(req, res) => {
  let sql = "SELECT * FROM users WHERE id='"+req.params.id+"'";

  let query = conn.query(sql, (err, results) => {
    if(err) throw err;

    res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
  });
});
 
//Server listening
app.listen(3002,() =>{
  console.log('Server started on port 3002...');
});